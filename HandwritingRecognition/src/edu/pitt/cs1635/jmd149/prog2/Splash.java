package edu.pitt.cs1635.jmd149.prog2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.RelativeLayout;


public class Splash extends Activity {

RelativeLayout rl; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
	
		
		Thread timer = new Thread(){
			
			public void run(){
				try{
					sleep(4000);
				}catch (InterruptedException e){
					e.printStackTrace();
					
				}finally{
					
					Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog2.HOMESCREEN");
					startActivity(openStartingPoint);
					
				}
				
			}			
			
		};
		timer.start();		
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();		
		finish();
	}
	
	

}

