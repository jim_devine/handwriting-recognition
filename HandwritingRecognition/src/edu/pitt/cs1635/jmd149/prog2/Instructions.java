package edu.pitt.cs1635.jmd149.prog2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Instructions extends Activity {

Button back;
TextView tv1, tv2, tv3, tv4;
ImageView im1, im2, im3;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.instructions);
		
		
		
		tv1 = (TextView)findViewById(R.id.ins_text1);
		tv2 = (TextView)findViewById(R.id.ins_text2);
		tv3 = (TextView)findViewById(R.id.ins_text3);
		tv4 = (TextView)findViewById(R.id.ins_text4);
		
		tv1.setText("1) Choose color from the menu at the bottom of the screen");
		tv2.setText("2) Draw an English or Chinese character onto the screen");
		tv3.setText("3) Click \"submit\" to see the possible characters");
		tv4.setText("4) Touch anywhere on the screen to close the returned characters" +
				"\n\n 5) Click \"Clear\" to erase screen to draw another character");
		
		back = (Button)findViewById(R.id.back);
		back.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog2.HOMESCREEN");
				startActivity(openStartingPoint);
				
			}
		});
	}

	

}
