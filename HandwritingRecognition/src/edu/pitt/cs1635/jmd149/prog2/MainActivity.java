package edu.pitt.cs1635.jmd149.prog2;




import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;


public class MainActivity extends Activity implements AdapterView.OnItemSelectedListener {
	
	WritingSurface ourSurfaceView;
	Button clear;
	Spinner spinner;
	RelativeLayout top;
	Paint paint;
	Button done;
	AlertDialog signal;
	private AlertDialog.Builder builder;
	Button back;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);		
		setContentView(R.layout.activity_main);		
		top = (RelativeLayout)findViewById(R.id.rl);	
		top.setBackgroundColor(Color.BLACK);	
		builder = new AlertDialog.Builder(this);
		 signal = builder.create();
		ourSurfaceView = (WritingSurface)findViewById(R.id.view1);
		ourSurfaceView.setBackgroundColor(Color.WHITE);
		
		back = (Button)findViewById(R.id.save);
		clear = (Button)findViewById(R.id.clear);
		paint = new Paint(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(4);
		spinner = (Spinner)findViewById(R.id.spinner);
		done = (Button)findViewById(R.id.done);
		
		ArrayAdapter adapter =ArrayAdapter.createFromResource(this, R.array.colors, android.R.layout.simple_spinner_item);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);		
		
		
		back.setOnClickListener( new View.OnClickListener() {  //sends back to homescreen
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				
				Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog2.HOMESCREEN");
				startActivity(openStartingPoint);
				
				
			}
		});
		
		
		
	
		done.setOnClickListener(new View.OnClickListener() { //sends the drawing to the server and displays response
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				String key= "11773edfd643f813c18d82f56a8104ed";
				 // Creating HTTP client
		        HttpClient httpClient = new DefaultHttpClient();
		        // Creating HTTP Post
		        HttpPost httpPost = new HttpPost("http://cwritepad.appspot.com/reco/gb2312");
		        
		        List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
		        
		        nameValuePair.add(new BasicNameValuePair("key", key));
				
				nameValuePair.add(new BasicNameValuePair("q", getQ()));
		        
		        try {
		        	
		        	httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
		            HttpResponse response = httpClient.execute(httpPost);		        	
		            
		        	signal.setMessage(EntityUtils.toString(response.getEntity()));
		        	signal.show();
		        	signal.setCanceledOnTouchOutside(true);
		        	
		        } catch (ClientProtocolException e) {
		            e.printStackTrace();
		        } catch (IOException e) {
		            e.printStackTrace();
		 
		        }
		        
		    }

			public String getQ() { //makes the encoding
				String q = "[";
				boolean firstPoint = true;
				for(Point point : ourSurfaceView.list) {
					if(firstPoint) {
						
						int width = (int)((point.x/ourSurfaceView.getMeasuredWidth())*254.0f);
						int height = (int)((point.y/ourSurfaceView.getMeasuredHeight())*254.0f);
						q += width + ", " + height;
						
					}
					else  {
						
						int width = (int)((point.x/ourSurfaceView.getMeasuredWidth())*254.0f);
						int height = (int)((point.y/ourSurfaceView.getMeasuredHeight())*254.0f);
						q +=  ", " + width + ", " + height;
						
						
					}
					if(point.end){
							q += ", 255, 0"; 							
						}
					firstPoint = false;
				}
				
				if(!q.equals("["))
					q += ", 255, 255";
				
				q += "]";
				return q;
			}
		
			
		});
		
		
		clear.setOnClickListener(new View.OnClickListener() { //clears the writing surface

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				ourSurfaceView.clear();
			}
			
			
		});
		
	
		ourSurfaceView.setOnTouchListener(new OnTouchListener() {	//saves where the user touches to an array list to be drawn	
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				float x = event.getX();
				float y = event.getY();
				
					
				if(event.getAction() == MotionEvent.ACTION_DOWN){
					Paint color = new Paint(paint);
						Point p = new Point(x, y, color, true, false);						
						ourSurfaceView.list.add(p);
				}
						
				else if(event.getAction() == MotionEvent.ACTION_UP){
					Paint color = new Paint(paint);
						Point p2 = new Point(x,y, color, false,true);
						ourSurfaceView.list.add(p2);
				}
				else{
					Paint color = new Paint(paint);
					Point p3 = new Point(x,y, color, false,false);
					ourSurfaceView.list.add(p3);
				}									
				ourSurfaceView.invalidate();
				return true;
			} 	});
	
		
	}

	//color choices
	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		TextView myText = (TextView)arg1;
		if(myText.getText().toString().equals("Yellow")){
			paint.setColor(Color.YELLOW);
		}
		else if(myText.getText().toString().equals("Blue")){
			paint.setColor(Color.BLUE);
		}
		else if(myText.getText().toString().equals("Green")){
			paint.setColor(Color.GREEN);
		}
		else if(myText.getText().toString().equals("Red")){
			paint.setColor(Color.RED);
		}
		else if(myText.getText().toString().equals("Black")){
			paint.setColor(Color.BLACK);
		}
		else if(myText.getText().toString().equals("Cyan")){
			paint.setColor(Color.CYAN);
		}
		else if(myText.getText().toString().equals("Magenta")){
			paint.setColor(Color.MAGENTA);
		}
		else if(myText.getText().toString().equals("Gray")){
			paint.setColor(Color.GRAY);
		}
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		// TODO Auto-generated method stub
		
	}

	

}
