package edu.pitt.cs1635.jmd149.prog2;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

public class WritingSurface extends View  {

	Canvas canvas;
	ArrayList<Point> list;
	public WritingSurface(Context context, AttributeSet a) {
		// TODO Auto-generated constructor stub
		super(context, a);
		list = new ArrayList<Point>();
						
	}	

	public void clear(){
		list.clear();
		this.invalidate();
		
	}
	
	public void setList(ArrayList<Point> l){
		list = l;
		
	}

	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		for(int i = 0; i < list.size() -1; i++){
			Point a = list.get(i);
			Point b = list.get(i + 1);
			
			if(a.start && b.end){ //if the user just touches/clicks the screen once
				canvas.drawPoint(a.x, a.y, a.paint);
			}			
		}		
		float sX = 0;
		float sY = 0;
		
		for(Point point : list){			
			
			if(point.start){	//starts a new stroke				
				sX = point.x;
				sY = point.y;				
			}
			else{ //draws the stroke as the user moves finer/mouse
				canvas.drawLine(sX, sY, point.x, point.y, point.paint);
				sX = point.x;
				sY = point.y;
			}				
		}	
	}
	
	


		
		
	
	

	

}

