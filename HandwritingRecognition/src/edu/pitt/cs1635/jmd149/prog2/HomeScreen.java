package edu.pitt.cs1635.jmd149.prog2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeScreen extends Activity {
Button draw;
Button ins;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home_screen);
		draw = (Button)findViewById(R.id.draw);
		ins = (Button)findViewById(R.id.inst);
		
		draw.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog2.MAINACTIVITY");
				startActivity(openStartingPoint);
				
			}
		})	;
		
		ins.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent openStartingPoint = new Intent("edu.pitt.cs1635.jmd149.prog2.INSTRUCTIONS");
				startActivity(openStartingPoint);
			}
		});
		
	}

}
